Présentation du projet:

    Voici mon dossier (e-portfolio) personnel qui rassemble de façon organisée mes travaux, mes compétences 
    et mon parcours au travers d'un support numérique.


Fonctionnement:

    Pour ce faire, j'ai choisit de développer le portfolio sur une seule page, répartie en 5 parties : l'entête, les projets, 
    les compétences et technologies, le parcours, et enfin le formulaire de contact.


Maquette: https://www.figma.com/file/9H7RgoV2bY2Mz3F1NOC2AN/Untitled?node-id=0%3A1

Lien vers site : http://portfoliocsth.surge.sh/
